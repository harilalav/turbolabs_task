from django.contrib import admin
from django.views.static import serve
from django.conf import settings as SETTINGS
from django.urls import path, include, re_path


urlpatterns = [
    path('admin/', admin.site.urls),

    path('', include('web.urls', namespace='web')),

    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': SETTINGS.MEDIA_ROOT}),
    re_path(r'^static/(?P<path>.*)$', serve, {'document_root': SETTINGS.STATIC_FILE_ROOT}),
]
