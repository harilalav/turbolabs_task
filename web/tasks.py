# Default imports
import os
import csv
from sre_constants import SUCCESS
# Django imports
from django.conf import settings as SETTINGS
# Celery imports
from celery import states
from celery import shared_task
from celery.exceptions import Ignore
from celery.result import AsyncResult
from celery.signals import task_success, task_failure
# Custom imports
from web.models import CeleryLog
from web.functions import generate_unique_id, create_celery_log_instance


@shared_task(bind=True)
def generate_file(self, file_name, data_count):
    task_id = self.request.id

    if not file_name.lower().endswith('.csv'):
        file_name = f"{file_name}.csv"

    if type(data_count) is not int:
        try:
            data_count = int(data_count)
        except:
            print("FATAL!!..Unable to convert!!")
            self.update_state(
                state = states.FAILURE,
                meta = {
                    'custom':'Unable to convert to int'
                }
            )
            # raise Ignore()

    path = 'media/data/'
    file_url = os.path.join("data/", file_name)

    # open the file in the write mode
    if not os.path.exists(path):
        os.makedirs(path)
    
    with open(f'{path}{file_name}', 'w', encoding='UTF8') as f:
        # create the csv writer
        writer = csv.writer(f)

        for i in range(0, data_count):
            writer.writerow(generate_unique_id())
    
    task_state = AsyncResult(task_id).state

    # Create the model instance with PENDING status
    create_celery_log_instance(task_id, task_state, file_name, data_count, file_url)
        
    return  file_url


@task_success.connect(sender=generate_file)
def task_success_notifier(sender=None, **kwargs):
    task_id = generate_file.request.id
    task_state = AsyncResult(task_id).state

    # Update the mode instance with SUCCESS status
    CeleryLog.objects.filter(celery_task_id=task_id).update(status=task_state)

    print("Instance created successfully")


@task_failure.connect(sender=generate_file)
def task_failure_notifier(sender=None, **kwargs):
    task_id = generate_file.request.id
    task_state = AsyncResult(task_id).state
    file_url = None
    file_name = generate_file.request.args[0]
    data_count = generate_file.request.args[1]

    if not file_name.lower().endswith('.csv'):
        file_name = f"{file_name}.csv"
        
    if type(data_count) is not int:
        data_count = None

        # Update model instance if it is created before
        if CeleryLog.objects.filter(celery_task_id=task_id).exists():
            CeleryLog.objects.filter(celery_task_id=task_id).update(status=task_state)
        else:
            # Create model instance if any error happened and instance was not created.
            create_celery_log_instance(task_id, task_state, file_name, data_count, file_url)

        print("Instance created successfully")