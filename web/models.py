from django.db import models


class CeleryLog(models.Model):
    celery_task_id = models.CharField(max_length=128)
    status = models.CharField(max_length=128)
    file_name = models.CharField(max_length=128, null=True)
    data_count = models.PositiveIntegerField(null=True)
    file_url = models.FileField(null=True)

    class Meta:
        db_table = 'web_celery_log'
        verbose_name = 'Celery Log'
        verbose_name_plural = 'Celery Logs'

    def __str__(self):
        return self.celery_task_id