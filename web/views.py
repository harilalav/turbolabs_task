from rest_framework.permissions import AllowAny
from rest_framework.decorators import permission_classes
from django.http.response import HttpResponse


@permission_classes([AllowAny])
def index(request):

	return  HttpResponse("Turbolabs Celery Task")
	