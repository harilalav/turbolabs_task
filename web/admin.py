from django.contrib import admin
from web.models import CeleryLog


class CeleryLogAdmin(admin.ModelAdmin):
    list_display = ('celery_task_id', 'status', 'file_name','data_count',)
    search_fields = ('celery_task_id',)
    
admin.site.register(CeleryLog, CeleryLogAdmin)
