# Default imports
import string
import random
# Custom imports
from web.models import CeleryLog


def generate_unique_id(size=80, chars=string.ascii_lowercase + string.digits):
    
    return ''.join(random.choices(chars, k=size))    


def create_celery_log_instance(task_id, task_state, file_name, data_count, file_url):
    celery_log = CeleryLog.objects.create(
        celery_task_id = task_id,
        status = task_state,
        file_name = file_name,
        data_count = data_count,
        file_url = file_url,
    )

    return celery_log
